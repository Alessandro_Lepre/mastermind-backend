﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MasterMind.Data.DomainClasses
{
    public class Game : IGame
    {
        private int _currentRound;

        public Guid Id { get; set; }
        public GameSettings Settings { get; set; }
        public IList<IPlayer> Players { get; }
        public string[] PossibleColors { get; }
        public int CurrentRound => _currentRound;


        /// <summary>
        /// Constructs a Game object and generates a code to guess.
        /// </summary>
        public Game(GameSettings settings, IList<IPlayer> players)
        {
            //TODO
        }

        public CanGuessResult CanGuessCode(IPlayer player, int roundNumber)
        {
            //TODO
            throw new NotImplementedException();
        }

        public GuessResult GuessCode(string[] colors, IPlayer player)
        {
            //TODO
            throw new NotImplementedException();
        }

        public GameStatus GetStatus()
        {
            //TODO
            throw new NotImplementedException();
        }
    }
}