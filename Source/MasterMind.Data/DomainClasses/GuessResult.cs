﻿using System.Collections.Generic;
using System.Linq;

namespace MasterMind.Data.DomainClasses
{
    public class GuessResult
    {
        private int _correctColorAndPositionAmount;
        private int _correctColorAmount;

        public string[] Colors { get; set; }

        public virtual int CorrectColorAndPositionAmount => _correctColorAndPositionAmount; //must be virtual for some automated test to work!

        public virtual int CorrectColorAmount => _correctColorAmount; //must be virtual for some automated test to work!

        public GuessResult(string[] colors)
        {
            //TODO
        }

        public void Verify(string[] codeToGuess)
        {
            //TODO
        }
    }
}